package be.kdg.layouts

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class GridLayoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_layout)

        addEventhandlers()
    }

    private fun addEventhandlers() {
        val closeButton = findViewById<Button>(R.id.close_button)
        closeButton.setOnClickListener {
            finish()
        }
    }
}
