package be.kdg.layouts

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addEventHandlers()
    }

    private fun addEventHandlers() {
        val openRelativeLayout = findViewById<Button>(R.id.open_relative_layout)
        openRelativeLayout.setOnClickListener {
            startActivity(Intent(this, RelativeLayoutActivity::class.java))
        }

        val openLinearLayout = findViewById<Button>(R.id.open_linear_layout)
        openLinearLayout.setOnClickListener {
            startActivity(Intent(this, LinearLayoutActivity::class.java))
        }

        val openGridLayout = findViewById<Button>(R.id.open_grid_layout)
        openGridLayout.setOnClickListener {
            startActivity(Intent(this, GridLayoutActivity::class.java))
        }

        val openConstraintLayout = findViewById<Button>(R.id.open_constraint_layout)
        openConstraintLayout.setOnClickListener {
            startActivity(Intent(this, ConstraintLayoutActivity::class.java))
        }
    }
}
